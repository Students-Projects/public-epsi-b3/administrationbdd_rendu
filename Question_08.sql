CREATE TABLE hr.client(
	id INT PRIMARY KEY NOT NULL, 
	nom VARCHAR(32), 
	prenom VARCHAR(32), 
	dateNaissance DATE, 
	Adresse VARCHAR(128),
	cp INT,
	ville VARCHAR(32)
);
